# Dutch GNOME translation team

Welcome to the Dutch GNOME translation team!

## Issues

Did you spot a mistake in the translation of GNOME software? Please [report your issue](https://gitlab.gnome.org/Teams/Translation/nl/issues/new) on Gitlab. 

## Help translate

Would you like to help translating GNOME software? Great! We use [Damned Lies](https://l10n.gnome.org/teams/nl/) for managing translations. All information needed to start translating can be found in the [Wiki](https://wiki.gnome.org/GnomeNederlands) (Dutch).
